site_name: 'AGSi Guidance'
site_url: 'https://ags-data-format-wg.gitlab.io/AGSi_Documentation'

site_description: 'AGSi Documentation'
site_author: 'AGS DMWG'

theme:
  name: 'material'
  palette:
    primary: 'light green'
    accent: 'light green'
  logo: 'images/AGS-logo.png'
  favicon: 'images/AGS-logo.png'
  feature:
    tabs: false

extra:
  font: false

# Copyright
copyright: 'Copyright &copy; 2022 AGS'


extra_css:
    - 'stylesheets/extra.css'

nav:
- 'Home': 'index.md'
- 'Standard': https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/
- 'Guidance':
   - 'General':
      - 'Introduction': 'Guidance_Intro.md'
      - 'How to read AGSi schema diagrams': 'Guidance_General_UML.md'
      - 'Governance - Version control': 'Guidance_General_Revision.md'
      - 'Governance - Naming conventions': 'Guidance_General_Naming.md'
      - 'Governance - Authoring Guidance': 'Guidance_General_Authoring.md'
   - 'Introduction to AGSi':
       - 'What is AGSi?': 'Guidance_Brief_Scope.md'
       - 'Beginners guide to object models and JSON': 'Guidance_Brief_General.md'
       - 'The AGSi schema': 'Guidance_Brief_Schema.md'
   - 'Models':
      - 'Models overview': 'Guidance_Model_General.md'
      - 'Volume models': 'Guidance_Model_Volumes.md'
      - 'Volume models - Worked example': 'Guidance_Model_Volumes_Example.md'
      - 'Models showing exploratory holes': 'Guidance_Model_Boreholes.md'
      - 'Models showing exploratory holes - Worked example': 'Guidance_Model_Boreholes_Example.md'
      - 'Models using sections/fence/profile diagrams': 'Guidance_Model_Sections.md'
   - 'Data':
      - 'Data general guidance': 'Guidance_Data_General.md'
      - 'Data - Worked example': 'Guidance_Data_Example.md'
   - 'Encoding':
      - 'Intro to JSON': 'Guidance_Encoding_Json.md'
   - 'Example files':
      - 'Silvertown tunnel': Example_Silvertown.md

markdown_extensions:
    - admonition
    - footnotes
    - codehilite
    - pymdownx.highlight
    - pymdownx.inlinehilite
    - pymdownx.superfences
    - pymdownx.snippets
    - toc:
        permalink: true
