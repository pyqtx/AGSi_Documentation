# AGSi Guidance / Data

## Data - Worked example

This example shows how to transfer a summary of parameter information as typically found in a ground investigation interpretative and/or geotechnical design report.

A copy of the schedule that is being represented is shown below.

![Data example - input schedule](./images/Guidance_Data_Example_Schedule.png)

An abbreviated (cut down) version is shown and described below, with commentary. This cut down version covers only the data in the yellow shaded
boxes in the schedule.

Full example data files for both the full and cut down versions can be downloaded from
[here](./exampledata/AGSi_Guidance_Examples.zip).

!!! Note
    In the following the JSON data is broken up in to several sections to allow commentary to be provided. To ensure that the JSON data is formatted correctly in the browser code block it has been necessary to include an additional "{" or "[" at the top and/or bottom of some of the sections. For the correct version of full JSON file please use the link posted above.

Properties, i.e. summary of observations, are not included in this example.
For properties the principles and basic structure are similar, but different objects which have different attributes are used.

Data objects must be embedded in Model or Observation Group objects. For the purposes of this example, the parameters are embedded in model elements
(<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a> objects).
However,  in the interests of brevity, the geometry and other attribute data for the model elements has been omitted.

#### Parent object (agsiModel & agsiModelElement)

We start with the parent model. This is an <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel">agsiModel</a> object that resides within the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Root_Intro">root object</a>. Schema rules dictate that this model object must be within an array (multiple models are permitted in <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Root_Intro">root</a>) despite us having only one model object in this example.

We then add, as a child of the parent model object, our first model element
(<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a> object), in this case representing Made Ground. Multiple model elements are allowed and so this is the first item in an array.

!!! Note
    The order of the model elements is not relevant in AGSi.

``` json
    {
        "agsiModel": [
    		{
    			"description": "Model with example data for Data example (cut down version)",
    			"remarks": "Model metadata and geometry data omitted for brevity",
    			"agsiModelElement": [
    				{
    					"elementID": "MG",
    					"elementName": "Made Ground",

}]}]}
```

#### Parameter values

We now define the parameters for Made Ground using an array of <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue">agsiDataParameterValue</a> objects.
This array is embedded under the
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement#agsidataparametervalue">*agsiDataParameterValue*</a> attribute of the parent <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a>.


Each parameter value object has a code defined using the
*<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue#codeid">codeID</a>*
attribute (the codes will be defined later) and a parameter value defined using the
*<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue#valuenumeric">valueNumeric</a>*
attribute. Some parameters also have a 'case' defined via the
*<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue#caseid">caseID</a>*
attribute (case IDs are also defined later).

``` json
                    {
                        "agsiDataParameterValue": [
                            {
                                "codeID": "UnitWeightBulk",
                                "valueNumeric": 19
                            },
                            {
                                "codeID": "AngleFrictionPeak",
                                "caseID": "CharacteristicLow",
                                "valueNumeric": 25
                            },
                            {
                                "codeID": "AngleFrictionPeak",
                                "caseID": "CharacteristicHigh",
                                "valueNumeric": 30
                            },
                            {
                                "codeID": "AngleFrictionCritical",
                                "caseID": "CharacteristicLow",
                                "valueNumeric": 25
                            },
                            {
                                "codeID": "AngleFrictionCritical",
                                "caseID": "CharacteristicHigh",
                                "valueNumeric": 30
                            }
                        ]
                    },

```

#### Design lines

Here is another set of parameters, for Gotham Clay. This includes an
example of how a design line is defined making use of the
*<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue#valueprofileindvarcodeid">valueProfileIndVarCodeID</a>*
and
*<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue#valueprofile">valueProfile</a>*
 attributes.

``` json

                    ,{
                        "elementID": "GC",
                        "elementName": "Gotham Clay",
                        "agsiDataParameterValue": [
                            {
                                "codeID": "UnitWeightBulk",
                                "valueNumeric": 20
                            },
                            {
                                "codeID": "UndrainedShearStrength",
                                "caseID": "EC7characteristic",
                                "valueProfileIndVarCodeID": "Elevation",
                                "valueProfile": [
                                    [6.0,110.0],
                                    [-28.3,329.52]
                                ]
                            },
                            {
                                "codeID": "UndrainedShearStrength",
                                "caseID": "LDSAmean",
                                "valueProfileIndVarCodeID": "Elevation",
                                "valueProfile": [
                                    [6.0,130.0],
                                    [-28.3,378.675]
                                ]
                            }
                        ]
                    }
```

#### Codes

To complete the data, we need to define the codes used.

At present, it is considered to be best practice to define the codes within the data as described below, even if the codes used are from of the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Codes_Codelist">AGSi standard code list</a>.

!!! Note
    An early beta version of AGSi had *agsiDataCode* and *agsiDataCase* objects in the Data group to define property and parameter codes. These have now been deleted with
    <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProjectCode">agsProjectCode</a> to be used instead.

First, there must be a parent <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProject">agsProject</a> object, which sits alongside <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel">agsiModel</a> in the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Root_Intro">root object</a>. For brevity, only a project name and remarks are included here.

All codes defined must be part of a parent <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProjectCodeSet">agsProjectCodeSet</a> object, so we define this first.
To avoid ambiguity, the object and attribute using this set of codes are identified here. A link to the source of the code list should also be provided, if applicable.


``` json
    {
        "agsProject": {
            "projectName": "C999 Geotechnical Package X",
            "remarks": "Other project metadata omitted for brevity",
            "agsProjectCodeSet": [
                {
                    "description": "Parameter codes (AGSi)",
                    "usedByObject": "agsiDataParameterValue",
                    "usedByAttribute": "codeID",
                    "sourceDescription": "AGSi standard code list",
                    "sourceURI": "https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Codes_Codelist/",
}]}}
```

The code definitions are provided as an array of <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProjectCode">agsProjectCode</a> objects embedded under the
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProjectCodeSet#agsprojectcode">*agsProjectCode*</a>
attribute of the parent
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProjectCodeSet">agsProjectCodeSet</a>.

``` json
                {
                    "agsProjectCode": [					
                        {
                            "codeID": "Elevation",
                            "description": "Elevation",
                            "units": "mOD",
                            "isStandard": true
                        },
                        {
                            "codeID": "UnitWeightBulk",
                            "description": "Bulk unit weight",
                            "units": "kN/m3",
                            "isStandard": true
                        },
                        {
                            "codeID": "AngleFrictionPeak",
                            "description": "Peak effective angle of shearing resistance",
                            "units": "deg",
                            "isStandard": true
                        },
                        {
                            "codeID": "AngleFrictionCritical",
                            "description": "Critical state effective angle of shearing resistance",
                            "units": "deg",
                            "isStandard": true
                        },
                        {
                            "codeID": "UndrainedShearStrength",
                            "description": "Undrained shear strength",
                            "units": "kPa",
                            "isStandard": true
                        }
                    ],
                },

```

#### Cases

The codes used for the case IDs are similarly defined as a new set of codes, which in this case are bespoke to the project (most likely for case IDs).

``` json
                {
                    "description": "Parameter case codes",
                    "usedByObject": "agsiDataParameterValue",
                    "usedByAttribute": "caseID",
                    "agsProjectCode": [
                        {
                            "codeID": "CharacteristicLow",
                            "description": "Characteristic low value",
                            "remarks": "Use when low value critical for design"
                        },
                        {
                            "codeID": "CharacteristicHigh",
                            "description": "Characteristic high value",
                            "remarks": "Use when high value critical for design"
                        },
                        {
                            "codeID": "EC7characteristic",
                            "description": "EC7 characteristic"
                        },
                        {
                            "codeID": "LDSAmean",
                            "description": "Mean for LDSA pile design"
                        }
                    ]
                }

```

Full example data files for both the full and cut down versions can be downloaded from
[here](./exampledata/AGSi_Guidance_Examples.zip).

### Version history

First formal issue, for v1.0.0 of standard, 30/11/2022.
