# AGSi Guidance / General

## How to read AGSi schema diagrams

AGSi documentation sometimes uses diagrams to illustrate how the schema is structured.
These diagrams are based on <a href="https://github.com/ISO-TC211/UML-Best-Practices/wiki/Introduction-to-UML" target="_blank">UML class diagram methodology</a>,
but with a few simplifications to make them more accessible to non-data specialists.

Here we describe how the AGSi schema diagrams work so that you will be able to understand them a little better.

There are two basic types of diagram that you need to understand, as described below.

!!! Note
    Most diagrams are curtailed by scope, eg. only including objects from a particular group and excluding links to objects in other parts of the schema. The scope should be clearly apparent from the context or associated notes.

### Summary diagrams

AGSi schema summary diagrams include only object names and parent-child relationships, as illustrated by the example below. Attributes and cross-reference relationships are not shown.

<br>

![Guidance on summary UML diagram](./images/Guidance_UML_Summary.svg)

<br>

### Full UML diagrams

Full AGSi UML diagrams include object attributes and all relationships, within diagram scope,
as illustrated by the example below.

<br>

![Guidance on full UML diagram](./images/Guidance_UML_Full.svg)

<br>

In practice, many of the 'full' diagrams provided in the documentation are a hybrid of summary and full diagrams as they often incorporate only summary representation for some parts of the schema.

### Notes on AGSi implementation of UML diagrams

When researching how best to present our schema diagrams, the authors came across a few subtly different implementations of UML class diagrams. Rather than getting bogged down with figuring out which was the 'correct' implementation, we decided to go with an implementation that seemed the best fit to our needs.

Our implementation is inspired by <a href="http://geosciml.org/" target="_blank">GeoSciML</a>, an OGC standard from our domain!

!!! Note
    Select the link for the *UML summary wall poster* to see the GeoSciML full UML diagram. This makes AGSi look very simple!

We have adopted some additional style rules to make our diagrams a bit easier to follow, or so we hope! The main ones are:

- parent objects always located above child objects in the diagram
- parent-child (composition) links run from bottom of parent to top of child, and these have thick lines
- cross-reference links run from sides of relevant objects and have thin lines

### Version history

First formal issue, for v1.0.0 of standard, 30/11/2022.
