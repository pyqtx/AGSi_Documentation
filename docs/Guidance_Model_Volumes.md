# AGSi Guidance / Models

## Volume models


3D volume models are expected to be one of the most common applications of AGSi.
This includes 3D geological models and geotechnical design models
as well as similar models in other <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#domain-of-model">domains</a>.
Examples of how to form some of these are provided below.

!!! Note
    In the documentation we have adopted the term **volume model** for models formed using 3D volumetric or solid elements. *Solid* may be the more 'correct' term to use but *volume* model is commonly used and understood, despite the term *volume* also being used as a measure of the size of the volume.

![3D geological model](./images/Guidance_Model_Volume_3D.png)


### Geological model using surfaces - principles

A <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#geological-model">geological model</a>, more correctly defined as an
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#engineering-geology-observational-model">engineering geology observational model</a>,
normally comprises volumes that each represent a geological unit. The vertical extents of the units are normally derived from interpolation between known observation points such as boreholes.

These units are often defined using surfaces from which the volumes
associated with the units are implied. AGSi supports this method, as
described in the following. The alternative of using explicit volume geometry is described towards the
[end of this page](#geological-model-using-explicit-volume-geometry).

!!! Note
    Use of explicit volumetric elements (e.g. using STEP, STL file formats) to represented solid/volumetric elements in a model is preferred. However, not all users have access to software capable of producing or receiving explicit volume geometry. Use of surfaces from which volumes are implied remains commonplace at present, despite its limitations. Therefore AGSi supports both methods. However, it is hoped that use of explicit volume geometry will become more commonplace over time.


![3D model from surfaces](./images/Guidance_Model_Volume_Surfaces.png)

Surfaces describing the top and/or bottom of units may be curved, e.g. derived using surface fitting methods, or formed of adjoining planar surfaces, e.g. triangular irregular network (TIN) surfaces. Different software packages may use different methods.

For AGSi it is recommended that both top and bottom surfaces are defined for each unit. This should remove any ambiguity regarding the extent of the unit in question.

!!! Note
    If only one surface (top or bottom) is defined it is assumed that the unit extends, up or down as applicable, to the next surface. In such cases there is potential for error if the top/bottom surface for an intermediate unit is incorrectly omitted from the model. Such errors may not be obvious and may remain undetected. If both top and bottom are included then all
    units should be defined correctly. In this case, if a unit is omitted there would be a visible gap in the model.

For simple layered models the bottom of one unit may be the same as the top of another. In such cases it is possible to define one surface that can be used for both.
However, in cases where some layers are intermittent, the bottom of one unit may interface with more than one underlying unit. In such cases it will be necessary to construct a separate bottom of unit surface.

The treatment of intermittent layers will require careful consideration by the specifier and/or modeller. Two possible approaches, illustrated below, are:

1.  Surfaces for an intermittent unit are extended beyond the true extent of the unit. However, coincident surfaces are defined in areas where the unit is not present, i.e. modelled unit has zero thickness.
2. Surfaces for intermittent unit are curtailed to the extent of the intermittent unit.

![Intermittent units](./images/Geometry_Rules_Intermittent.svg)

!!! Note
    If Method 2 is used, a separate model element will be required for each occurrence of the intermittent unit. Method 1 allows use of one model element for all occurrences.

For a lens the method of interpretation used renders it necessary to split the enclosing unit into separate model elements representing the volumes above and below the lens. A variant of method 1 may be used as shown in the diagram below:

![Lens using method 1](./images/Geometry_Rules_Lens.svg)

!!! Note
    The above solution for a lens is not ideal as it creates an artificial split in the unit. Explicit volume modelling is likely to provide a more elegant solution for lenses.

Sometimes the boreholes themselves are included in the model.
For guidance on how to achieve this see
[Guidance - Models showing exploratory holes](./Guidance_Model_Boreholes.md)

### Geological model using surfaces - use of schema

A single <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#model">model</a> is established by defining an <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel">agsiModel</a> object, which will
contain general metadata for the model.

A <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#model-boundary">model boundary</a>
may also be defined using an
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelBoundary">agsiModelBoundary</a>
object embedded using the
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel#agsimodelboundary">*agsiModelBoundary*</a>
attribute of <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel">agsiModel</a>.

!!! Note
    The model boundary acts as a 'cookie-cutter' to the model elements (described below),
    i.e. any model element geometry straying beyond the model boundary is ignored.

The model is then constructed using
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#model-element">model elements</a>
which are defined using <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a> objects.
Typically, each geological unit will be one model element, although as discussed above this may not always be the case, especially with units that are not continuous. All of the model elements are embedded, as an array, within the
*<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel#agsimodelelement">agsiModelElement</a>*
attribute of the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel">agsiModel</a> object.

The geometry for each <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a> object is defined by embedding an object from the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_Intro">Geometry group</a> under the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement#agsigeometry">*agsiGeometry*</a> attribute. For geological units defined using surfaces this should be an
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryVolFromSurfaces">agsiGeometryVolFromSurfaces</a> object.

If AGSi recommendations are followed both top and bottom surfaces will be defined for each unit in the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryVolFromSurfaces">agsiGeometryVolFromSurfaces</a> object using attributes
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryVolFromSurfaces#agsigeometrytop">*agsiGeometryTop*</a> and
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryVolFromSurfaces#agsigeometrybottom">*agsiGeometryBottom*</a>
respectively. Each of these should contain an embedded
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a> object
(<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryPlane">agsiGeometryPlane</a> is a valid alternative but this will only be applicable to flat plane surfaces, e.g. the bottom of a model).

The embedded <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>
objects will identify the location of the
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#agsi-supporting-files">supporting file</a>
containing the surface geometry, e.g. DWG, DGN, IFC, WKT or LandXML file. File metadata may also be included (recommended). Further information relating to file formats is given in <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_Rules">Standard /  Geometry / Rules and conventions</a>.

It is recommended that <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#agsi-supporting-files">supporting</a>
geometry files are <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#agsi-included-files">included</a> in the
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#agsi-package">AGSi package</a> with the
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#agsi-file">AGSi file</a>.
Use of a subfolder named *geometry* is recommended.
See <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Transfer">Standard / General / Transfer of AGSi</a> for details.

!!! Note
    References to other files in AGSi use the URI or URI-reference protocols as defined in <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Formats#uri">Standard: General rules and conventions</a>.
    If the file is included in the AGSi package then a relative reference (using <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#uri-reference">uri-reference</a>) should be provided.

Use of <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryVolFromSurfaces">agsiGeometryVolFromSurfaces</a>
clarifies how the surface(s) are to be put together and interpreted to form an implied volumetric model. Rules for interpretation are provided in
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_Rules">Standard: Geometry rules and conventions</a>.

If the user(s) require different rules for interpretation of surfaces then the
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryVolFromSurfaces">agsiGeometryVolFromSurfaces</a> object
shall not be used and the model elements shall directly reference
the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>
objects that define those surfaces. The project specific rules for interpretation shall be conveyed to other users via documentation, which should be referenced and included as part of the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#agsi-package">AGSi package</a>.

A diagram illustrating the relationships described above is shown at the
[bottom of this page](#diagram).

A simple example illustrating the above is given
[on the next page](./Guidance_Model_Volumes_Example.md).

### Geological model using explicit volume geometry

The process for using 'explicit' volume / solid geometry for the model is straightforward.

The volume geometry for each unit will be defined using an <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>
object, which will point to a file with geometry in a suitable format.

There will be only one volume geometry object for each
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_General_Definitions#model-element">model element</a>
and therefore the
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>
object referencing this file should be embedded directly in the applicable
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a> object, using the
<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement#agsigeometry">*agsiGeometry*</a>
attribute of course.


### Simple geotechnical design model using layers

Simple geotechnical design models that have horizontal layers,
ie. a stratigraphical column as illustrated below, can be defined very easily in AGSi. No supporting files are required.

![Simple layer model](./images/Guidance_Model_Volume_Layers.png)

The general process using <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel">agsiModel</a> and <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a> is as described above.

For the geometry of each geotechnical unit (layer) the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryLayer">agsiGeometryLayer</a>
object can be used. This has attributes for
*<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryLayer#topelevation">topElevation</a>* and
*<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryLayer#bottomelevation">bottomElevation</a>*. It is recommended that both of these are defined for each layer.
This object is embedded directly into <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a> using the <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement#agsigeometry">*agsiGeometry*</a> attribute.

A model simply defined as per the above may be interpreted as having no spatial location or dimensions in the XY plane, i.e. it is a one dimensional model (Z only). Model metadata may be used to define the validity of the model, which may include the location(s) that it applies to.

Alternatively, <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Geometry_agsiGeometryLayer">agsiGeometryLayer</a> objects can be interpreted as  representing a horizontal layer of infinite XY extent. It is
then possible to use a <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelBoundary">agsiModelBoundary</a> object to define a limiting valid area for each model.
Alternatively, the valid areas can be similarly limited for each model element using the
*<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement#agsigeometryarealimit">agsiGeometryAreaLimit</a>*
attribute of <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a>.

Layers with sloping surfaces cannot be modelled using this method.
For these it will necessary to use one of the methods described
above for the geological model.

A simple example illustrating the above is given
[on the next page](./Guidance_Model_Volumes_Example.md).

A diagram showing the above objects and their relationships is given below.

<a href="../images/Guidance_Obs_Volumes.svg" target="_blank">Click here to a open full size version of this diagram in a new browser window</a>.

<a name="diagram"></a>

!!! Note
    This diagram includes only the objects mentioned on this page. Relationships of these objects to other objects in the schema are not shown.

![Volume models extract](./images/Guidance_Obs_Volumes.svg)

### Version history

First formal issue, for v1.0.0 of standard, 30/11/2022.