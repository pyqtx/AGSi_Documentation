# AGSi Guidance / General

## Governance - Version control

Version control is in effect for this standard. The standard defines the schema. Therefore the version number of the schema is always consistent with the version number of the standard.

AGSi JSON Schema versioning shall follow the schema version, except that the JSON Schema version used will additionally be appended to the AGSi version number. For example:
`1.0.0+2019-09` is the JSON Schema for AGSi version 1.0.0 in accordance with version 2019-09 of the <a href="http://json-schema.org/specification.html" target="_blank">JSON Schema Specification</a>

The system used for version numbering broadly follows the principles of
<a href="https://semver.org/" target="_blank">Sematic Versioning</a>
which adopts the *Major.Minor.Patch* convention. For AGSi this has been interpreted thus:

* *Major* versions are reserved for major changes in scope or changes that break backwards compatibility
* *Minor* versions used when functionality is added in a backwards compatible manner
* *Patch* versions used for minor updates or correction of errors in documentation or the schema that do not change intent (includes new codes or vocabulary items)

Further notes:

* Addition of new codes and vocabulary items considered to be a minor change permissible as a *patch* version given that such codes/items are only recommended
* Inclusion of the `-beta` suffix is proposed at beta stage, as permitted by SemVer. However, this is not part of the formal numbering system, i.e. v0.5.0 and v0.5.0-beta should be considered to be the same.
* At beta stage we will stay at major version `0`. Version `1` will be reserved for the first formal release.
* Numbering of all levels starts at `0`. There are no leading zeros, but we can go into double digits if required, i.e. `0.12.101` is ok (but we hope we don't get to that!)
* During beta stage, we expect to be 'generous' when deciding what counts as ok for just a patch update as opposed to a minor version, ie. erring towards the former if in doubt. We will adopt a strict interpretation for version 1.0.0 and beyond.


Notes on timing of versions:

* Patch versions will only be issued when required, and generally no more frequent than once per month. An exception may be made if a problem arises that needs an urgent fix.
* There are currently no target dates for minor or major version releases. However, current thinking is that the gap between major versions is likely to be years, not months.

### Version history

First formal issue, for v1.0.0 of standard, 30/11/2022.
