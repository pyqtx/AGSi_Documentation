# -*- coding: utf-8 -*-
"""
Created on Fri Nov 11 21:46:48 2022

@author: Neil Chadwick
"""

# This is a script for what should be a one-off process, but we will keep it just 
# in case we need to do something similar in the future

# Same as the Guidance to Standard vresion - but instead looking at links
# to Guidance in the standard and changing those!


# I think the regex works ok - but will do manual checks on the output to make sure

import re
from tkinter import Tk, filedialog

#version = '0.6.2' not applicalble here

def regexreplace(match_obj):
    if match_obj.group() is not None:
        x = match_obj.group()
        # Now get link text and link
        linkparts = x.split('](./')
        linktext = linkparts[0].replace('[','')
        linkaddress = linkparts[1].replace(')','')
        linkaddress = linkaddress.replace('.md','')
        newlink = 'https://ags-data-format-wg.gitlab.io/AGSi_Documentation/' + linkaddress
        newhyperlink = '<a href="' + newlink + '">' + linktext + '</a>'
        print(newhyperlink)
        return newhyperlink
    
#Get input files

root = Tk()
inputfilelist=filedialog.askopenfilenames(initialdir = '',
                                    title = 'Select input files',
                                    filetypes = (('md files','*.md'),
                                                 ('text files','*.txt')))

root.destroy()

# Loop through files selected

count = 0

for inputfile in inputfilelist:

    #Correct file name hard way until fix found
    inputfilepathname = inputfile.replace('/','\\')

    # New files will overwrite!
    outputfilepathname = inputfilepathname
    
   
    # Open file and get text
    f = open(inputfilepathname, 'r')
    pagetext = f.read()  # This reads whole file
    f.close()  # Close file
    
    # This is the all important regex - I think it works ok!
    regexfind = r'\[+?.+?\]\(\./(Guidance).+?\.md.*?\)'
    
    # This does the replacement using the function above
    newpagetext = re.sub(regexfind, regexreplace, pagetext)
    
    #Write to file (overwrite!)  
    f = open(outputfilepathname, 'w')
    f.write(newpagetext)
    f.close()
    
    count = count + 1
    
print('Processed ' + str(count) + ' files')

