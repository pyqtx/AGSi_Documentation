# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 21:16:33 2019

@author: neil.chadwick


"""
#TODO
# chagne case of true/false for bool example

#TODO
#obj names italics missed in a few plages when in text
# other sheets
#where do we put extra guidance?

import openpyxl
import os
from tkinter import Tk, filedialog
import csv

from replacement import replaceObjectLink, replaceSimple


#Get input file
root = Tk()
inputfile=filedialog.askopenfilename(initialdir = '',
                                    title = 'Select input file',
                                    filetypes = (('Excel files','*.xls'),
                                                 ('Excel files','*.xlsx')))

#Correct file name hard way until fix found
inputfile = inputfile.replace('/','\\')
inputfolder = os.path.dirname(inputfile)
# Select folder to save as (will prompt if already exists)
outputfolder=filedialog.askdirectory(initialdir = '',
                                   title = 'Folder for md files created')


#Point to locations where list of object replacements found (CSV)
        
objectreplacementfile=filedialog.askopenfilename(initialdir = '',
                                    title = 'Select text/csv file with object link replacements',
                                    filetypes = (('csv','*.csv'),('Text files','*.txt')))

# Parse replacement list to array to be used
with open(objectreplacementfile,encoding ='utf-8-sig',newline='') as csvfile:
    objectreplacementlist = list(csv.reader(csvfile))

root.destroy()  # Gets rid of annoying Tk window

#Read workbook
wb = openpyxl.load_workbook(inputfile, data_only=True)
sheetnames = wb.sheetnames
# Read each sheet
for sheetname in sheetnames:
    outputtxt = '' #Initialise text back to blank
    txt = [] # Reset temp text array
    ws = wb[sheetname]
    if sheetname.startswith('ags'):  # If starts with ags then process as attribute table
        # What Group is it?
        if sheetname.startswith('agsiModel'):
            Group = 'Model'
        elif sheetname.startswith('agsiGeometry'):
            Group = 'Geometry'
        elif sheetname.startswith('agsiData'):
            Group = 'Data'
        elif sheetname.startswith('agsiObservation'):
            Group = 'Observation' 
        elif sheetname.startswith('agsProject'):
            Group = 'Project'
        elif sheetname == 'agsSchema' or sheetname == 'agsFile':
            Group = 'Root'
        else:
            Group = 'XXXXXX'
        # Get page number, and parse out top level (Group) number
        PageNr = str(ws.cell(row=1, column=2).value)
        GroupNr = PageNr.split('.')[0]
        agsobj = sheetname
        outputfilenameroot = 'Standard_' + Group + '_' # used later
        # Write top title lines
        txt.append('# AGSi Standard / ' + GroupNr + '. ' + Group + '\n')
        txt.append('## ' + PageNr + '. ' + agsobj + '\n')
        txt.append('### ' + PageNr + '.1. Object description' + '\n')
        
        # In the follwing, check col 1 is as expected - ignore if not
        if str(ws.cell(row=2, column=1).value).lower().startswith('description'):
            txt.append(str(ws.cell(row=2, column=2).value) + '\n')
        if str(ws.cell(row=3, column=1).value).lower().startswith('parent'):
            parent = str(ws.cell(row=3, column=2).value)
            if parent != 'None':
                parentlist = parent.split(',')
                if len(parentlist) == 1:
                    txt.append('The parent object of ' + agsobj + ' is ' + parent + '\n')
                else:
                    temptxt = ' may be embedded in any of the following potential parent objects: '
                    txt.append(agsobj + temptxt + '\n\n- ' + '\n- '.join(parentlist) + '\n')                   
        if str(ws.cell(row=4, column=1).value).lower().startswith('child'):
            child = str(ws.cell(row=4, column=2).value)
            if child != 'None':
                child = child.replace(', ',',')
                childlist = child.split(',')
                temptxt = ' contains the following embedded child objects: '
                txt.append(agsobj + temptxt + '\n\n- ' + '\n- '.join(childlist) + '\n')
        if str(ws.cell(row=5, column=1).value).lower().startswith('association'):
            assoc = str(ws.cell(row=5, column=2).value)
            if assoc != 'None':
                assoc = assoc.replace(', ',',')
                assoclist = assoc.split(',')
                temptxt = ' has associations (reference links) with the following objects: '
                txt.append(agsobj + temptxt + '\n\n- ' + '\n- '.join(assoclist) + '\n')
        if str(ws.cell(row=6, column=1).value).lower().startswith('attribute'):
            # This is the header row for the table, triggers table loop
            # For now we assume correct order of columns!

            txt.append(agsobj + ' has the following attributes:\n\n')
            # This next bit is a clunky way of doing it, but its a late chagne so easier 
            # to do it this way right now!
            # First the summary list at the top
            r = 7
            attr = str(ws.cell(row=r, column=1).value)
            while attr != 'None':  #Do until blank attribute name reached!
                #anchor link must be all lower case. No spaces or symbols expected.
                attrlink = attr.lower()
                txt.append('- [' + attr + '](#' + attrlink + ')' )
                r = r + 1
                if r > 50:  #emergency stop!
                    break
                attr = str(ws.cell(row=r, column=1).value)
            txt.append('\n')
            
            # Now the full attribute descriptions
            txt.append('### ' + PageNr + '.2. Attributes' + '\n')
            r = 7
            attr = str(ws.cell(row=r, column=1).value)
            while attr != 'None':  #Do until blank attribute name reached!
                atype = str(ws.cell(row=r, column=2).value)
                addreq = str(ws.cell(row=r, column=3).value)
                isreq = str(ws.cell(row=r, column=4).value)
                desc = str(ws.cell(row=r, column=5).value)
                # example needs downcasing if boolean
                if atype == 'boolean':
                    example = str(ws.cell(row=r, column=6).value).lower()
                else:
                    example = str(ws.cell(row=r, column=6).value)
                temptxt = '#### '+ attr + '\n'
                temptxt = temptxt + desc + '  \n'
                temptxt = temptxt + '*Type:* ' + atype
                if addreq != 'None':
                    temptxt = temptxt + ' (' + addreq + ')'
                temptxt = temptxt + '  \n'
                # Next section for enum only
                if 'from list below' in addreq:
                    enumdata = str(ws.cell(row=r, column=8).value)
                    enumlist = enumdata.split(',')
                    for enumitem in enumlist:
                       temptxt = temptxt + '\- ' + enumitem + '  \n'
                elif 'from reference below' in addreq: # Don't think this is used any more. Keep just in case.
                    enumdata = str(ws.cell(row=r, column=9).value)
                    if enumdata == 'None':
                        enumdata = 'reference link TBC'
                    temptxt = temptxt + '\- ' + enumdata + '  \n'
                # Carry on
                if isreq != 'None':
                    temptxt = temptxt + '*Condition:* ' + isreq + '  \n'
                if example != 'None':
                    temptxt = temptxt + '*Example:* ``' + example + '``'
                txt.append(temptxt +'\n')
                r = r + 1
                attr = str(ws.cell(row=r, column=1).value)


        # Compile to single text file 
        txt.append('')
        outputtxt = outputtxt + '\n' + '\n'.join(txt)
        
        # Make replacements here - line by line allows us to weed out #
        # First do the simple replacements
        # Note - if there are objects, DO NOT try to replace here, to avoid double jeopardy!
        # First get the replacement from the apreadsheet - the sheet called replace
        simplereplacementlist = []
        ws = wb['replace']
        r = 1
        oldtext = str(ws.cell(row=r, column=1).value)
        while oldtext != 'None':
            newtext = str(ws.cell(row=r, column=2).value)
            pair = [oldtext, newtext]
            simplereplacementlist.append(pair)
            r = r + 1
            oldtext = str(ws.cell(row=r, column=1).value)
        # Now call the replace function
        outputtxt = replaceSimple(outputtxt,simplereplacementlist) 
                
                
        # Now do the object replacements
        outputtxt = replaceObjectLink(outputtxt,objectreplacementlist) 
                      
    
        

        # Save as md
        outputfilename = outputfilenameroot + sheetname + '.md'
        outputfile = os.path.join(outputfolder, outputfilename)
        f = open(outputfile, 'w')
        f.write(outputtxt)
        f.close()
        
print('Done!')